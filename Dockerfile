ARG ALPINE_VERSION=3.16
ARG GOLANG_VERSION=1.19

FROM golang:${GOLANG_VERSION}-alpine${ALPINE_VERSION} AS builder

ARG LF_VERSION=27
ARG PISTOL_VERSION=0.3.2

RUN apk add --no-cache \
        build-base \
        curl \
        file-dev \
        tar && \
    curl -fLo pistol.tar.gz \
        https://github.com/doronbehar/pistol/archive/refs/tags/v${PISTOL_VERSION}.tar.gz && \
    curl -fLo lf.tar.gz \
        https://github.com/gokcehan/lf/archive/refs/tags/r${LF_VERSION}.tar.gz && \
    tar xzf pistol.tar.gz && tar xzf lf.tar.gz && \
    cd pistol-${PISTOL_VERSION} && \
    env CGO_ENABLED=1 GO111MODULE=on go build \
        -ldflags="-s -w -X main.Version=v${PISTOL_VERSION}-git" \
        -v -o /go/bin/pistol ./cmd/pistol && \
    cd ../lf-r${LF_VERSION} && \
    env CGO_ENABLED=0 go build -ldflags="-s -w" -v -o /go/bin/lf


FROM alpine:${ALPINE_VERSION}

COPY --from=builder /go/bin/pistol /usr/local/bin/
COPY --from=builder /go/bin/lf /usr/local/bin/
RUN apk add --no-cache \
        file \
        mediainfo \
        netcat-openbsd \
        poppler-utils && \
    chmod +x /usr/local/bin/pistol && \
    chmod +x /usr/local/bin/lf

CMD ["lf"]
